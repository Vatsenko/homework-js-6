"use strict"
//1. Це методи щоб працювати з об'єктами
//2.Властивість є значення або набір значень у вигляді масиву або об'єкта, який є членом об'єкта і може містити будь який тип даних.
//3.Посилальний тип це коли працюємо з посиланням на об'єкт а не з його значенням

//1.

const product = {
    name: 'Bread',
    price: 150,
    discount: 50,

    getPriceWithDiscount() {
        return this.price - (this.price * this.discount / 100);
    }
};

console.log(`Повна ціна товару з урахуванням знижки: ${product.getPriceWithDiscount()}`);

//2.
function createGreeting(obj) {
    return "Hello, i`m " + obj.name + ",i`m " + obj.age + " years old";
}

const userName = prompt("Введіть ваше ім'я:");
const userAge = prompt("Введіть ваш вік:");

const userObject = {
    name: userName,
    age: userAge
};

alert(createGreeting(userObject));

//3.
function deepClone(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    let clone;

    if (Array.isArray(obj)) {
        clone = [];
        for (let i = 0; i < obj.length; i++) {
            clone[i] = deepClone(obj[i]);
        }
    } else {
        clone = {};
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                clone[key] = deepClone(obj[key]);
            }
        }
    }

    return clone;
}

const originalObject = {
    name: 'Тест',
    details: {
        age: 25,
        hobbies: ['спорт', 'читання']
    }
};

const clonedObject = deepClone(originalObject);
console.log(clonedObject);
